﻿public static class Constants {

	public const float FADE = 0.5f;
	public const float RESSURECTION_TIME = 1f;
	public const float ASTEROID_FIELD_TIME = 3f;
	public const int EXTRA_LIFE_SCORE = 1000;
	public const int SPAWN_UFO_SCORE = 2500;
	public const float EPSILON = 0.0001f;

	public static class Angle {
		public const float SPAWN_ANGLE_OFFSET = 45f;
		public const float DEGREE_360 = 360f;
		public const float RADIAN_TO_DEGREE = 57.29578f;
		public const float UP_TO_RIGHT_ANGLE_OFFSET = 90f;
	}

	public static class Settings {
		public const string MUSIC_KEY = "MusicVolume";
		public const string SFX_KEY = "SFXVolume";
	}

	public static class Scene {
		public const string MENU = "Menu";
		public const string GAMEPLAY = "Gameplay";
	}

	public static class Tag {
		public const string PLAYER = "Player";
		public const string LIMIT = "Limit";
		public const string BULLET = "Bullet";
		public const string ENEMY = "Enemy";
		public const string BORDER = "Border";
	}
}