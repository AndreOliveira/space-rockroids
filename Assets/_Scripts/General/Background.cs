﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

	[SerializeField] private float minSpeed = 0.5f;
	[SerializeField] private float maxSpeed = 1.5f;

	void Start () {
		Animator[] stars = GetComponentsInChildren<Animator>();
		for (int i = 0; i < stars.Length; i++)
		{
			stars[i].speed = Random.Range(minSpeed,maxSpeed);
		}
	}
	
}
