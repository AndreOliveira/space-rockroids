﻿using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "Space Rockroids/Settings", order = 2)]
public class ScriptableSettings : ScriptableObject {
	public float MusicVolume = 1f;
	public float SFXVolume = 1f;
}