﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {
	[SerializeField] private ScriptableGameEvent _onChangeScene;
	public static SceneChanger Instance { get; private set; }

	private void Start() {
		Instance = this;
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void Restart(){
		LoadScene(SceneManager.GetActiveScene().name);
	}

	public void LoadScene (string sceneName) {
		AsyncOperation async = SceneManager.LoadSceneAsync (sceneName);
		async.allowSceneActivation = false;
		StartCoroutine (ChangeScene (async));
	}

	private IEnumerator ChangeScene (AsyncOperation async) {
		_onChangeScene.Call ();
		yield return new WaitForSecondsRealtime (Constants.FADE);
		async.allowSceneActivation = true;
	}
}