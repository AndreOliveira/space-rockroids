﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOManager : MonoBehaviour {

	[Header ("UFO")]
	[SerializeField] private GameObject _ufo;
	[SerializeField] private int _amount;
	[SerializeField] private Transform _ufoParent;

	[Header ("Left Zone")]
	[SerializeField] private Transform _leftBeginZone;
	[SerializeField] private Transform _leftEndZone;

	[Header ("Right Zone")]
	[SerializeField] private Transform _rightBeginZone;
	[SerializeField] private Transform _rightEndZone;

	private PoolManager _ufoPool;

	// Use this for initialization
	void Start () {
		_ufoPool = new PoolManager (_amount, _ufo, _ufoParent);
	}

	public void SpawnUFO () {
		bool side = Random.Range (0, 10) % 2 == 0;

		float x = side ?
			Random.Range (_leftBeginZone.position.x, _leftEndZone.position.x) :
			Random.Range (_rightBeginZone.position.x, _rightEndZone.position.x);

		float y = side ?
			Random.Range (_leftBeginZone.position.y, _leftEndZone.position.y) :
			Random.Range (_rightBeginZone.position.y, _rightEndZone.position.y);

		Vector2 pos = new Vector2 (x, y);

		_ufoPool.Spawn(pos, 0);
	}
}