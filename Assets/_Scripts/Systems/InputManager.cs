﻿using UnityEngine;

public class InputManager {

	public float RotateAxis { get; private set; }
	public float AccelerateAxis { get; private set; }
	public bool hyperSpeed { get; private set; }
	public bool Shoot { get; private set; }

	public InputManager () {
		RotateAxis = 0f;
		AccelerateAxis = 0f;
		Shoot = false;
	}

	public void Update () {
		RotateAxis = Input.GetAxis ("Horizontal");

		Shoot = Input.GetButtonDown ("Fire1");

		float verticalAxis = Input.GetAxis ("Vertical");
		hyperSpeed = Input.GetButtonDown("Vertical") && verticalAxis <= Constants.EPSILON;
		if (!hyperSpeed)
			AccelerateAxis = verticalAxis;
	}
}