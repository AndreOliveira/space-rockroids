﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour {

	[Header ("Asteroid Field")]
	[SerializeField] private int _startAsteroidAmount = 3;
	[SerializeField] private Transform _beginLeftField = null;
	[SerializeField] private Transform _endLeftField = null;
	[SerializeField] private Transform _beginRightField = null;
	[SerializeField] private Transform _endRightField = null;

	[Header ("Small Gray Asteroid")]
	[SerializeField] private int _graySmallAmount = default (int);
	[SerializeField] private GameObject _graySmallPrefab = null;
	[SerializeField] private Transform _graySmallParent = null;

	[Header ("Medium Gray Asteroid")]
	[SerializeField] private int _grayMediumAmount = default (int);
	[SerializeField] private GameObject _grayMediumPrefab = null;
	[SerializeField] private Transform _grayMediumParent = null;

	[Header ("Big Gray Asteroid")]
	[SerializeField] private int _grayBigAmount = default (int);
	[SerializeField] private GameObject _grayBigPrefab = null;
	[SerializeField] private Transform _grayBigParent = null;

	[Header ("Small Brown Asteroid")]
	[SerializeField] private int _brownSmallAmount = default (int);
	[SerializeField] private GameObject _brownSmallPrefab = null;
	[SerializeField] private Transform _brownSmallParent = null;

	[Header ("Medium Brown Asteroid")]
	[SerializeField] private int _brownMediumAmount = default (int);
	[SerializeField] private GameObject _brownMediumPrefab = null;
	[SerializeField] private Transform _brownMediumParent = null;

	[Header ("Big Brown Asteroid")]
	[SerializeField] private int _brownBigAmount = default (int);
	[SerializeField] private GameObject _brownBigPrefab = null;
	[SerializeField] private Transform _brownBigParent = null;

	private PoolManager _smallGrayPool;
	private PoolManager _mediumGrayPool;
	private PoolManager _bigGrayPool;

	private PoolManager _smallBrownPool;
	private PoolManager _mediumBrownPool;
	private PoolManager _bigBrownPool;

	public List<GameObject> _asteroids;

	public static AsteroidManager Instance { get; private set; }

	private void Awake () {
		if (Instance)
			Destroy (gameObject);
		else
			Instance = this;
	}

	private void Start () {
		_asteroids = new List<GameObject> ();

		_smallGrayPool = new PoolManager (_graySmallAmount, _graySmallPrefab, _graySmallParent);
		_mediumGrayPool = new PoolManager (_grayMediumAmount, _grayMediumPrefab, _grayMediumParent);
		_bigGrayPool = new PoolManager (_grayBigAmount, _grayBigPrefab, _grayBigParent);

		_smallBrownPool = new PoolManager (_brownSmallAmount, _brownSmallPrefab, _brownSmallParent);
		_mediumBrownPool = new PoolManager (_brownMediumAmount, _brownMediumPrefab, _brownMediumParent);
		_bigBrownPool = new PoolManager (_brownBigAmount, _brownBigPrefab, _brownBigParent);

		StartCoroutine (CreateAsteroidField (_startAsteroidAmount, Time.fixedDeltaTime));
	}

	private IEnumerator CreateAsteroidField (int amount, float waitingTime) {
		yield return new WaitForSeconds (waitingTime);
		for (int i = 0; i < amount; i++) {
			float random = Random.Range (0f, 1f);

			bool side = Random.Range (0, 10) % 2 == 0;

			float x = side ?
				Random.Range (_beginLeftField.position.x, _endLeftField.position.x) :
				Random.Range (_beginRightField.position.x, _endRightField.position.x);

			float y = side ?
				Random.Range (_beginLeftField.position.y, _endLeftField.position.y) :
				Random.Range (_beginRightField.position.y, _endRightField.position.y);

			Vector2 pos = new Vector2 (x, y);

			if (random < 0.5f)
				_asteroids.Add (_bigGrayPool.Spawn (pos, Random.Range (0, 360f)));
			else
				_asteroids.Add (_bigBrownPool.Spawn (pos, Random.Range (0, 360f)));
		}
	}

	public void SpawnSubAsteroid (Vector2 position, ScriptableAsteroid asteroid, float angleMin, float angleMax) {
		PoolManager manager;

		//select the right asteroid to spawn
		if (asteroid.AsteroidPrefab.Equals (_smallBrownPool.PooledObject))
			manager = _smallBrownPool;
		else if (asteroid.AsteroidPrefab.Equals (_smallGrayPool.PooledObject))
			manager = _smallGrayPool;
		else if (asteroid.AsteroidPrefab.Equals (_mediumBrownPool.PooledObject))
			manager = _mediumBrownPool;
		else
			manager = _mediumGrayPool;

		//set how many sub asteroids will be spawned
		int amount = Random.Range (2, 4);

		//set a small gap between them
		float gap = 0.5f;
		float offset = -gap * amount / 2f;

		//spawn subasteroids
		for (int i = 0; i < amount; i++) {
			Vector2 spawnPosition = position;
			spawnPosition.x += offset;
			_asteroids.Add (manager.Spawn (spawnPosition, Random.Range (angleMin, angleMax)));

			offset += gap;
		}
	}

	public void CheckAsteroidField () {
		for (int i = 0; i < _asteroids.Count; i++) {
			if (!_asteroids[i].activeSelf)
				_asteroids.RemoveAt (i);
		}

		if (_asteroids.Count <= 0) {
			StartCoroutine (CreateAsteroidField (++_startAsteroidAmount, Constants.ASTEROID_FIELD_TIME));
		}
	}
}