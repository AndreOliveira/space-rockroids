﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "Space Rockroids/GameEvent", order = 0)]
public class ScriptableGameEvent : ScriptableObject {
	private List<GameEventListener> listeners = new List<GameEventListener>();

	public void Call() {
		for (int i = 0; i < listeners.Count; i++)
		{
			listeners[i].OnEventCalled();
		}
	}

	public void AddListener(GameEventListener listener) {
		listeners.Add(listener);
	}

	public void RemoveListener(GameEventListener listener) {
		listeners.Remove(listener);
	}
} 