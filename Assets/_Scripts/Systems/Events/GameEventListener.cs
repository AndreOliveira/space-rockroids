﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour {
	[SerializeField] private ScriptableGameEvent _gameEvent = null;
	[SerializeField] private UnityEvent _response = null;

	private void OnEnable() {
		_gameEvent.AddListener(this);
	}

	private void OnDisable() {
		_gameEvent.RemoveListener(this);
	}

	public void OnEventCalled() {
		_response.Invoke();
	}

}