﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class Score : MonoBehaviour {

	[SerializeField] private ScriptableScore score;
	private Text scoreText;

	private void Start () {
		scoreText = GetComponent<Text> ();
		score.ResetScore();
		
		scoreText.text = score.Value.ToString ();
	}

	public void UpdateUI () {
		scoreText.text = score.Value.ToString ();
	}

}