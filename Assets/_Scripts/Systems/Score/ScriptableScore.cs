﻿using UnityEngine;

[CreateAssetMenu (fileName = "Score", menuName = "Space Rockroids/Score", order = 0)]
public class ScriptableScore : ScriptableObject {

	public int Value;
	public int HighScore;
	[SerializeField] private ScriptableGameEvent _onUpdateScore;
	[SerializeField] private ScriptableGameEvent _onGetExtraLife;
	[SerializeField] private ScriptableGameEvent _onSpawnUFO;

	private int _lifeScore = 0;
	private int _UFOScore = 0;

	public bool CheckNewHighScore () {
		bool beatHighScore = Value > HighScore;

		if (beatHighScore)
			HighScore = Value;

		return beatHighScore;
	}

	public void AddScore (int amount) {
		Value += amount;
		_lifeScore += amount;
		_UFOScore += amount;
		_onUpdateScore.Call ();

		if (_lifeScore > Constants.EXTRA_LIFE_SCORE) {
			_lifeScore -= Constants.EXTRA_LIFE_SCORE;
			_onGetExtraLife.Call ();
		}

		if (_UFOScore > Constants.SPAWN_UFO_SCORE) {
			_UFOScore -= Constants.SPAWN_UFO_SCORE;
			_onSpawnUFO.Call ();
		}

		Debug.Log(_UFOScore);
		
	}

	public void ResetScore () {
		Value = _lifeScore = _UFOScore = 0;
	}

}