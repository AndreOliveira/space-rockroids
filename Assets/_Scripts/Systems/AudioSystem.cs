﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSystem : MonoBehaviour {

	[SerializeField] private AudioMixer _mixer = null;
	[SerializeField] private ScriptableSettings _settings = null;

	private AudioSource _musicSource;
	private Dictionary<string, float> _audioDictionary;

	#region MonoBehaviour methods
	private void Awake () {
		_audioDictionary = new Dictionary<string, float> ();
	}

	private void Start () {
		_musicSource = GetComponent<AudioSource> ();

		//Add music and sfx keys if they doesn't exists
		if (!_audioDictionary.ContainsKey (Constants.Settings.MUSIC_KEY)) {
			_audioDictionary.Add (Constants.Settings.MUSIC_KEY, _settings.MusicVolume);
		}
		if (!_audioDictionary.ContainsKey (Constants.Settings.SFX_KEY)) {
			_audioDictionary.Add (Constants.Settings.SFX_KEY, _settings.SFXVolume);
		}

		//increase music and sfx volume from 0 to maximum determinated on options
		StartCoroutine (AwakeAudio (Constants.Settings.MUSIC_KEY, _settings.MusicVolume));
		StartCoroutine (AwakeAudio (Constants.Settings.SFX_KEY, _settings.SFXVolume));
	}
	#endregion

	#region sleep-awake
	public void Sleep () {
		StartCoroutine (SleepAudio (Constants.Settings.MUSIC_KEY));
		StartCoroutine (SleepAudio (Constants.Settings.SFX_KEY));
	}

	private IEnumerator AwakeAudio (string volumeKey, float volumeTarget) {
		//not zero to avoid problems with decibels conversion
		float vol = Constants.EPSILON;

		//increase audio with time until hit expected value
		while (Math.Abs (vol) < volumeTarget) {
			vol += Time.fixedDeltaTime;
			SetAudioVolume (volumeKey, vol);
			yield return new WaitForFixedUpdate ();
		}

		//update audio to exactly expected
		//audio to avoid some minor error
		SetAudioVolume (volumeKey, volumeTarget);
	}

	private IEnumerator SleepAudio (string volumeKey) {
		float vol = _audioDictionary[volumeKey];

		//decrease audio to zero 
		while (vol > Constants.EPSILON) {
			float factor = Time.fixedDeltaTime / Constants.FADE;

			//don't allow volume be 0 to avoid problems with decibels converstion
			vol -= Utils.Clamp (vol - factor, Constants.EPSILON, _settings.MusicVolume);
			SetAudioVolume (volumeKey, vol);
			yield return new WaitForFixedUpdate ();
		}
		SetAudioVolume (volumeKey, Constants.EPSILON);
	}
	#endregion

	#region audio handle
	//the math here is to make the right converstion to decibels
	public void GameOverMusic () {
		StartCoroutine (SlowAudio ());
	}

	public void UpdateAudio () {
		SetAudioVolume (Constants.Settings.MUSIC_KEY, _settings.MusicVolume);
		SetAudioVolume (Constants.Settings.SFX_KEY, _settings.SFXVolume);
	}

	private void SetAudioVolume (string key, float value) {
		_audioDictionary[key] = value;
		float newVolume = (float) Math.Log10 (value) * 20f;
		_mixer.SetFloat (key, newVolume);
	}

	private IEnumerator SlowAudio () {
		//slow down the audio speed to make a
		//better game feel for game over time
		while (_musicSource.pitch > Constants.FADE) {
			_musicSource.pitch -= Time.fixedDeltaTime / Constants.FADE;
			yield return new WaitForFixedUpdate ();
		}
	}
	#endregion
}