﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	[SerializeField] private GameObject _pauseScreen;
	[SerializeField] private GameObject _confirmationScreen;
	private bool _goRestart;

	#region MonoBehaviour Methods
	private void Start () {
		Time.timeScale = 1f;
		_pauseScreen.SetActive (false);
		_confirmationScreen.SetActive (false);
	}

	private void OnDestroy() {
		Time.timeScale = 1f;
	}
	#endregion

	#region Menu Behaviours
	public void PauseUnpause () {
		Time.timeScale = Time.timeScale == 0f ? 1f : 0f;
		_pauseScreen.SetActive (!_pauseScreen.activeSelf);
	}

	public void Restart () {
		_goRestart = true;
		_confirmationScreen.SetActive (true);
	}

	public void MainMenu () {
		_goRestart = false;
		_confirmationScreen.SetActive (true);
	}

	public void Confirmed () {
		if (SceneChanger.Instance) {
			string scene = _goRestart ?
				SceneManager.GetActiveScene ().name :
				Constants.Scene.MENU;
			SceneChanger.Instance.LoadScene (scene);
		}
	}

	public void NotConfirmed () {
		_confirmationScreen.SetActive (false);
	}
	#endregion
}