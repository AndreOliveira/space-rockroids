﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct LifeSystem {

	private int _amount;
	private List<Image> _lives;

	public LifeSystem (int startLives, Image[] livesImages) {
		_amount = startLives;
		_lives = new List<Image> (livesImages);

		UnityEngine.Assertions.Assert.IsTrue (_amount <= _lives.Count, "No life images enough");

		for (int i = 0; i < _lives.Count; i++) {
			_lives[i].color = (i < _amount) ? Color.white : Color.black;
		}

	}

	/// <summary>
	/// If has slots (Images) to be filled, add a single life
	/// </summary>
	public void AddLife () {
		if (_amount < _lives.Count) {
			_lives[_amount].color = Color.white;
			_amount++;
		}
	}

	/// <summary>
	/// Remove a single life
	/// </summary>
	/// <returns>Returns true if it was the last life</returns>
	public bool RemoveLife () {
		if (_amount > 0) {
			_amount--;
			_lives[_amount].color = Color.black;
			return false;
		}
		return true;
	}
}