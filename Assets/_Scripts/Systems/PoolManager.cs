﻿using System.Collections.Generic;
using UnityEngine;

public class PoolManager {
	private GameObject _pooledObject;
	private int _amount = default (int);
	private GameObject[] _pool = null;

	public PoolManager (int amount, GameObject prefab, Transform poolParent = null) {
		_pooledObject = prefab;
		_amount = amount;
		_pool = new GameObject[_amount];
		
		for (int i = 0; i < _amount; i++) {
			_pool[i] = MonoBehaviour.Instantiate (prefab, poolParent) as GameObject;
			_pool[i].gameObject.SetActive (false);
		}
	}

	public GameObject Spawn (Vector2 position, float angle) {
		//serach on pool for a disabled object to be spawned
		for (int i = 0; i < _amount; i++) {
			if (!_pool[i].activeInHierarchy) {
				_pool[i].transform.position = new Vector3 (position.x, position.y);
				_pool[i].transform.rotation = Quaternion.Euler(Vector3.forward * angle);
				_pool[i].SetActive (true);
				return _pool[i];
			}
		}
		return null;
	}

	public GameObject PooledObject {
		get { return _pooledObject; }
	}
}