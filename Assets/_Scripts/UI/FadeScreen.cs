﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Image))]
public class FadeScreen : MonoBehaviour {
	[SerializeField] private bool _startDarkAndFade = true;
	private Image _blackScreen;

	private void Start () {
		//setup black screen and prevent from
		//player click on others UI elements
		_blackScreen = GetComponent<Image> ();
		_blackScreen.raycastTarget = false;

		if (_startDarkAndFade) {
			_blackScreen.color = Color.black;
			Fade (false);
		} else {
			_blackScreen.color = Color.clear;
		}
	}

	public void Fade (bool darken) {
		float fadeFactor = darken ? 1f : 0f;
		_blackScreen.CrossFadeAlpha (fadeFactor, Constants.FADE, true);
	}
}