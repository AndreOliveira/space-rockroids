﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HighScore : MonoBehaviour {

	[SerializeField] private ScriptableScore _score = null;
	private Text _highScoreText;

	void Start () {
		_highScoreText = GetComponent<Text>();
		_highScoreText.text = _score.HighScore.ToString();
	}
}
