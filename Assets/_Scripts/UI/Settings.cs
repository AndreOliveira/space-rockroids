﻿using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

	[Header("Scriptable Elements")]
	[SerializeField] private ScriptableSettings _settings;
	[SerializeField] private ScriptableGameEvent _onAudioChange;

	[Header("UI Elements")]
	[SerializeField] private Slider _music;
	[SerializeField] private Slider _sfx;

	private void Start () {
		_music.value = _settings.MusicVolume;
		_sfx.value = _settings.SFXVolume;
	}

	public void SetMusicVolume() {
		_settings.MusicVolume = _music.value;
		_onAudioChange.Call();
	}

	public void SetSFXVolume() {
		_settings.SFXVolume = _sfx.value;
		_onAudioChange.Call();
	}
}
