﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

	[SerializeField] private Text _gameOverText;
	[SerializeField] private Text _scoreText;
	[SerializeField] private Text _highScoreText;
	[SerializeField] private ScriptableScore _score;

	public void UpdateUI() {
		if (_score.CheckNewHighScore()) {
			_gameOverText.text = "NEW RECORD!";
			_gameOverText.color = Color.yellow;
		}

		_scoreText.text = _score.Value.ToString();
		_highScoreText.text = _score.HighScore.ToString();
	}

}
