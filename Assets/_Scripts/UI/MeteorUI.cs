﻿
using UnityEngine;

public class MeteorUI : MonoBehaviour {

	[SerializeField] private Transform[] _asteroids;
	[SerializeField] private float _minSpeed = 1f;
	[SerializeField] private float _maxSpeed = 5f;

	private float[] _angularSpeed = null;

	void Start () {
		_angularSpeed = new float[_asteroids.Length];

		for (int i = 0; i < _asteroids.Length; i++)
		{
			float side = i % 2 == 0 ? 1f : -1f;
			_angularSpeed[i] = Random.Range(_minSpeed, _maxSpeed) * Time.fixedDeltaTime * side;
		}
	}
	
	void FixedUpdate () {
		for (int i = 0; i < _asteroids.Length; i++)
		{
			_asteroids[i].Rotate(0f,0f,_angularSpeed[i]);	
		}		
	}
}
