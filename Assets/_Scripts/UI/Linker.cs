﻿using UnityEngine;

public class Linker : MonoBehaviour {

	public void OpenURL (string url) {
		Application.OpenURL(url);
	}
}