﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Menu : MonoBehaviour {

	private Animator _currentAnimator;

	private void Start() {
		_currentAnimator = GetComponent<Animator>();
	}

	public void ChangeMenuScreen(Animator anim) {
		_currentAnimator.SetTrigger("Exit");
		anim.SetTrigger("Enter");

		_currentAnimator = anim;
	}

	public void ExitGame() {
		Application.Quit();
	}
}
