﻿using UnityEngine;

[CreateAssetMenu(fileName = "Asteroid", menuName = "Space Rockroids/Asteroid", order = 0)]
public class ScriptableAsteroid : ScriptableObject {
	[SerializeField] private GameObject _asteroidPrefab;
	[SerializeField] private Sprite[] _sprites = null;
	[SerializeField] private int _score = default(int);
	[SerializeField] private float _minForce = default(float);
	[SerializeField] private float _maxForce = default(float);
	[SerializeField] private ScriptableAsteroid _subAsteroid = null;

	public Sprite GetRandomSprite() {
		return _sprites[Random.Range(0,_sprites.Length)];
	}

	public GameObject AsteroidPrefab {
		get { return _asteroidPrefab; }
	}

	public float GetRandomForce() {
		return Random.Range(_minForce, _maxForce);
	}

	public ScriptableAsteroid SubAsteroid {
		get { return _subAsteroid; }
	}

	public int Score {
		get { return _score; }
	}

}