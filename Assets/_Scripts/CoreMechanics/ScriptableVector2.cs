﻿using UnityEngine;

[CreateAssetMenu(fileName = "Vector2", menuName = "Space Rockroids/Vector2", order = 0)]
public class ScriptableVector2 : ScriptableObject {
	public Vector2 Value;
}