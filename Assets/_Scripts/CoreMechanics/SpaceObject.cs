﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D), typeof (Collider2D))]
public class SpaceObject : MonoBehaviour {

	[Header ("Movement")]
	[SerializeField] private Vector3 _radius;

	protected Rigidbody2D _rb;
	protected Camera _cam;

	protected void Start () {
		_rb = GetComponent<Rigidbody2D> ();
		_cam = Camera.main;

		_rb.gravityScale = 0f;
	}

	protected void OnTriggerStay2D (Collider2D other) {
		if (other.CompareTag (Constants.Tag.BORDER)) {
			Vector3 positiveViewport = _cam.WorldToViewportPoint (transform.position + _radius);
			Vector3 negativeViewport = _cam.WorldToViewportPoint (transform.position - _radius);
			Vector2 pos = _rb.position;

			if (negativeViewport.x > 1f && _rb.velocity.x > 0f) {
				pos.x *= -1f;
				_rb.MovePosition (pos);
			} else if (positiveViewport.x < 0f && _rb.velocity.x < 0f) {
				pos.x *= -1f;
				_rb.MovePosition (pos);
			}
			if (negativeViewport.y > 1f && _rb.velocity.y > 0f) {
				pos.y *= -1f;
				_rb.MovePosition (pos);
			} else if (positiveViewport.y < 0f && _rb.velocity.y < 0f) {
				pos.y *= -1f;
				_rb.MovePosition (pos);
			}
		}
	}

}