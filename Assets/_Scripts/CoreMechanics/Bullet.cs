﻿using System.Collections;
using UnityEngine;

public class Bullet : SpaceObject {

	[SerializeField] private float _speed = 5f;
	[SerializeField] private float _activeTime = 5f;

	new private void Start () {
		base.Start ();
		_rb.drag = 0f;
		Init();
	}

	private void OnEnable () {
		if (_rb)
			Init ();
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (!other.CompareTag (Constants.Tag.BORDER)) {
			gameObject.SetActive (false);
		}
	}

	private IEnumerator Disable () {
		yield return new WaitForSeconds (_activeTime);
		gameObject.SetActive (false);
	}

	private void Init () {
		_rb.velocity = transform.up * _speed;
		StartCoroutine (Disable ());
	}
}