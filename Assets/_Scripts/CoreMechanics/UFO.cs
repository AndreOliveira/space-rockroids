﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UFO : SpaceObject {

	[Header("Movement")]
	[SerializeField] private float _speed = 5f;
	[SerializeField] private float _frequency = 0.1f;
	[SerializeField] private float _amplitude = 0.1f;

	[Header("Aim and Shoot")]
	[SerializeField] private ScriptableVector2 _playerPosition;
	[SerializeField] private float _shootMinTime = 0.5f;
	[SerializeField] private float _shootMaxTime = 1.5f;

	[Header("Bullet")]
	[SerializeField] private int _amount = 5;
	[SerializeField] private GameObject _enemyBulletPrefab = null;

	[Header("Score")]
	[SerializeField] private int _scoreAmount = 250;
	[SerializeField] private ScriptableScore _score = null;

	private SpriteRenderer _spriteRenderer;
	private Collider2D _collider;
	private PoolManager _bulletPool;
	private ParticleSystem _explosion;

	new private void Start() {
		base.Start();
		_bulletPool = new PoolManager(_amount, _enemyBulletPrefab);
		_collider = GetComponent<Collider2D>();
		_explosion = GetComponentInChildren<ParticleSystem>();
		_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
	}

	private void OnEnable() {
		StartCoroutine(Aim());
	}

	private void OnDisable() {
		StopAllCoroutines();
	}

	private void FixedUpdate () {
		float x = _speed * Time.fixedDeltaTime;
		float y = Time.fixedDeltaTime * (float) Math.Sin(Time.time * _frequency) * _amplitude;
		Vector2 nextPos = _rb.position + new Vector2(x,y);

		_rb.MovePosition(nextPos);
	}

	private IEnumerator Aim() {
		while(true) {
			yield return new WaitForSeconds(UnityEngine.Random.Range(_shootMinTime, _shootMaxTime));
			Shoot();
		}
	}

	private void Shoot() {
		float x = _playerPosition.Value.x - _rb.position.x;
		float y = _playerPosition.Value.y -_rb.position.y;

		float angle = (float) Math.Atan2(y, x) * Constants.Angle.RADIAN_TO_DEGREE;

		angle -= Constants.Angle.UP_TO_RIGHT_ANGLE_OFFSET;
		angle += UnityEngine.Random.Range(-Constants.Angle.SPAWN_ANGLE_OFFSET,Constants.Angle.SPAWN_ANGLE_OFFSET);
		
		_bulletPool.Spawn(_rb.position,angle);
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.CompareTag (Constants.Tag.BULLET)) {
			_explosion.Play();
			
			StartCoroutine(DefeatUFO());
			_score.AddScore (_scoreAmount);
		}
	}

	private IEnumerator DefeatUFO() {
		StopAllCoroutines();
		_spriteRenderer.enabled = false;
		_collider.enabled = false;

		yield return new WaitForSeconds(2f);

		_spriteRenderer.enabled = true;
		_collider.enabled = true;
		gameObject.SetActive (false);
	}
}
