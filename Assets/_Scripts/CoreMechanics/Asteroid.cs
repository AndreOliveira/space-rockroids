﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : SpaceObject {

	[SerializeField] private ScriptableAsteroid _asteroid;
	[SerializeField] private ScriptableScore _score;

	private SpriteRenderer _spriteRenderer;
	private ParticleSystem _explosion;
	private Collider2D _collider;

	new private void Start () {
		base.Start ();
		_collider = GetComponent<Collider2D>();
		_spriteRenderer = GetComponentInChildren<SpriteRenderer> ();
		_explosion = GetComponentInChildren<ParticleSystem>();
		Init ();
	}

	private void OnEnable () {
		if (_rb && _spriteRenderer) {
			Init ();
		}
	}

	private void Init () {
		_rb.AddForce (transform.up * _asteroid.GetRandomForce ());
		_rb.AddForce (transform.right * _asteroid.GetRandomForce () / 3);
		_spriteRenderer.sprite = _asteroid.GetRandomSprite ();

		float angle = Random.Range (0, Constants.Angle.DEGREE_360);
		_spriteRenderer.transform.rotation = Quaternion.Euler (Vector3.forward * angle);
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.CompareTag (Constants.Tag.BULLET)) {
			_explosion.Play();
			
			StartCoroutine(DefeatAsteroid());
			_score.AddScore (_asteroid.Score);

			if (_asteroid.SubAsteroid) {
				float angleMin = _rb.rotation - Constants.Angle.SPAWN_ANGLE_OFFSET;
				float angleMax = _rb.rotation + Constants.Angle.SPAWN_ANGLE_OFFSET;
				AsteroidManager.Instance.SpawnSubAsteroid (transform.position, _asteroid.SubAsteroid, angleMin, angleMax);
			}
		}
	}

	private IEnumerator DefeatAsteroid() {
		_spriteRenderer.enabled = false;
		_collider.enabled = false;

		yield return new WaitForSeconds(1f);

		_spriteRenderer.enabled = true;
		_collider.enabled = true;
		gameObject.SetActive (false);

		AsteroidManager.Instance.CheckAsteroidField();
	}
}