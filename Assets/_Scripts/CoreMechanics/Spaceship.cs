﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (CircleCollider2D))]
public class Spaceship : SpaceObject {

	#region properties
	[SerializeField] private float _force = 10f;
	[SerializeField] private float _maxSpeed = 10f;
	[SerializeField] private float _angularSpeed = 30f;

	[Header ("Weapon")]
	[SerializeField] private GameObject _bullet = null;
	[SerializeField] private Transform _bulletLoader = null;
	[SerializeField] private ScriptableGameEvent _shootEvent = null;
	[SerializeField] private int _amount = 10;

	[Header ("Lives")]
	[SerializeField] private bool _canInteract = default(bool);
	[SerializeField] private int _startLives = default(int);
	[SerializeField] private Image[] _lives = null;
	[SerializeField] private LayerMask _hurtfulLayers = default(LayerMask);
	[SerializeField] private ScriptableGameEvent _onGameOver = null;

	[Header("Spaceship")]
	[SerializeField] private ParticleSystem _trail;
	[SerializeField] private ParticleSystem _explosion;
	[SerializeField] private ScriptableVector2 _playerPosition;

	private InputManager _input;
	private PoolManager _bulletPool;
	private LifeSystem _lifeSystem;

	private SpriteRenderer _spriteRenderer;
	private CircleCollider2D _coll;

	#endregion

	#region MonoBehaviour Methods
	new private void Start () {
		base.Start ();
		_coll = GetComponent<CircleCollider2D> ();
		_trail = GetComponentInChildren<ParticleSystem>();
		_spriteRenderer = GetComponentInChildren<SpriteRenderer> ();
		_lifeSystem = new LifeSystem (_startLives, _lives);
		_input = new InputManager ();
		_bulletPool = new PoolManager (_amount, _bullet, _bulletLoader);
		_canInteract = true;
	}

	private void Update () {
		if (!_canInteract)
			return;

		_input.Update ();
		if (_input.Shoot) {
			if (_bulletPool.Spawn (_rb.position, _rb.rotation)) {
				_shootEvent.Call();
			}
		}

		if(_input.AccelerateAxis > 0 && !_trail.isPlaying) {
			_trail.Play();
		} else if (_input.AccelerateAxis <= 0 && _trail.isPlaying) {
			_trail.Stop();
		}

		_playerPosition.Value = _rb.position;
	}

	private void FixedUpdate () {
		if (!_canInteract)
			return;

		if (_input.AccelerateAxis > 0) {
			_rb.AddForce (transform.up * _force);
		}
		_rb.velocity = Vector2.ClampMagnitude(_rb.velocity, _maxSpeed);

		if (_input.hyperSpeed) {
			Vector3 randomViewport = new Vector3 (Random.Range (0f, 1f), Random.Range (0f, 1f), -_cam.transform.position.z);
			_rb.MovePosition (_cam.ViewportToWorldPoint (randomViewport));
			_rb.velocity = Vector2.zero;
		}

		_rb.MoveRotation (_rb.rotation - _input.RotateAxis * _angularSpeed);
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.CompareTag (Constants.Tag.ENEMY) || other.CompareTag (Constants.Tag.BULLET)) {
			_explosion.Play();
			if (_lifeSystem.RemoveLife ())
				GameOver ();
			else {
				StartCoroutine (Ressurect ());
			}
		}
	}
	#endregion

	#region Methods
	private IEnumerator Ressurect () {
		SetSpaceshipEnabled (false);

		_rb.MovePosition (Vector2.zero);
		_rb.velocity = Vector2.zero;
		yield return new WaitForSeconds (Constants.RESSURECTION_TIME);
		yield return new WaitUntil (() => !Physics2D.OverlapCircle (_rb.position, 2f, _hurtfulLayers));

		SetSpaceshipEnabled(true);
	}

	private void GameOver () {
		SetSpaceshipEnabled (false);
		_bulletLoader.gameObject.SetActive (false);
		_onGameOver.Call();
	}

	private void SetSpaceshipEnabled (bool enabled) {
		_spriteRenderer.enabled = enabled;
		_coll.enabled = enabled;
		_canInteract = enabled;
	}

	public void GetExtraLife() {
		_lifeSystem.AddLife();
	}
	#endregion
}